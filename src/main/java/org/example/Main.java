package org.example;

public class Main {
    public static void main(String[] args) {

        // First
        int[] fibonacci = new int[]{1, 1, 0};

        System.out.println("for:");
        System.out.print(fibonacci[0] + " " + fibonacci[1] + " ");
        for(int i = 0; i <= 8; i++) {

            fibonacci[2] = fibonacci[0] + fibonacci[1];
            fibonacci[0] = fibonacci[1];
            fibonacci[1] = fibonacci[2];

            System.out.print(fibonacci[2] + " ");
        }
        System.out.println();
        // Second
        System.out.println("while:");
        fibonacci = new int[]{1, 1, 0};
        int count = 0;

        System.out.print(fibonacci[0] + " " + fibonacci[1] + " ");
        while (count++ <= 8){
            fibonacci[2] = fibonacci[0] + fibonacci[1];
            fibonacci[0] = fibonacci[1];
            fibonacci[1] = fibonacci[2];

            System.out.print(fibonacci[2] + " ");
        }

    }
}